// Do not edit. Bootstrap copy of /home/bnat6582/go/src/cmd/internal/objabi/zbootstrap.go

//line /home/bnat6582/go/src/cmd/internal/objabi/zbootstrap.go:1
// auto generated by go tool dist

package objabi

import "runtime"

const defaultGOROOT = `/home/bnat6582/go`
const defaultGO386 = `sse2`
const defaultGOARM = `5`
const defaultGOOS = runtime.GOOS
const defaultGOARCH = runtime.GOARCH
const defaultGO_EXTLINK_ENABLED = ``
const version = `go1.9.2`
const stackGuardMultiplier = 1
const goexperiment = ``
